var path = require('path');
const webpack = require('webpack');
// для подключения css
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    entry: './client/client.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'assets/js')
    },
    plugins: [
        // NoErrorsPlugin — это стандартный плагин Webpack, который не дает перезаписать скрипты при наличии в них ошибок
        new webpack.NoErrorsPlugin(),
        // CommonsChunkPlugin — выносит общие библиотеки для чанков в отдельный чанк. Удобно для кеширования внешних библиотек и уменьшения веса чанков.
        new webpack.optimize.CommonsChunkPlugin({
            children: true,
            async: true,
        }),
        // DedupePlugin — находит общие зависимости библиотек и обобщает их
        new webpack.optimize.DedupePlugin(),
        // UglifyJsPlugin — плагин, который сжимает скрипты
        new webpack.optimize.UglifyJsPlugin(),
        new ExtractTextPlugin({
            filename: '../css/bundle.min.css',
            disable: false,
            allChunks: true
        }),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.min\.css$/,
            cssProcessorOptions: {
                discardComments: {
                    removeAll: true
                }
            }
        })
    ],
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
                presets: [
                    ['es2015', {
                        modules: false
                    }]
                ]
            }
        }, {
            test: /\.css$/,
            loader: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: 'css-loader'
            })
        }]
    }
}