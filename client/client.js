'use strict'
// Add all project styles
require('./css/style.css');

// Add the rest of JavaScript files
require('./js/main.js');
